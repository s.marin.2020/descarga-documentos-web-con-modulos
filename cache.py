from robot import Robot


class Cache:
    def __init__(self):
        self.cache = {}

    def retrieve(self, url):
        if url not in self.cache:
            robot = Robot(url)
            self.cache[url] = robot
            self.cache[url].retrieve()
    def show(self, url):
        self.retrieve(url)
        self.cache[url].show()

    def show_all(self):
        j = 1
        for i in self.cache.keys():
            print('Url ', j, ' :', i)
            j += 1

    def content(self, url):
        if url not in self.cache:
            self.retrieve(url)
        return self.cache[url].content()


if __name__ == '__main__':
    print('-----Comprobamos el uso de las funciones de esta clase-------')
    c = Cache()
    c.content('http://gsyc.urjc.es/')
    c.retrieve('http://gsyc.urjc.es/')
    c.show('https://www.aulavirtual.urjc.es')
    c.show_all()

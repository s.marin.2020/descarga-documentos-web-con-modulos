# Descarga Documentos Web con módulos

Repositorio de plantilla para el ejercicio "Descarga de documentos web con módulos". Recuerda que puedes consultar su enunciado en la guía de estudio (programa) de la asignatura.

Los pasos ha seguir son:  
1. Crear el archivo `robot.py`
2. Crear el archivo `cache.py`
3. Probar la funcionalidad de los archivos anteriores en `cache_web_modules.py`

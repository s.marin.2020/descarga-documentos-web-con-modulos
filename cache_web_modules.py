from cache import Cache

if __name__ == '__main__':
    print('Probamos la funcionalidad del programa')
    c1 = Cache()
    c2 = Cache()
    c1.content('https://www.urjc.es/')
    c2.retrieve('https://gsyc.urjc.es/')
    c1.show_all()
    c2.content('https://www.upm.es/')
    c2.show('https://www.urjc.es/')
    c2.show_all()
    c1.show('https://www.ucam.edu/')
    c1.show_all()

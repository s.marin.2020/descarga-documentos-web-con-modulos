import urllib.request


class Robot:

    def __init__(self, url: str = None):
        self.url = url
        self.memory = False

    def retrieve(self):
        if not self.memory:
            print('Descargando', format(self.url))
            self.memory = True
            f = urllib.request.urlopen(self.url)
            self.contenido = f.read().decode('utf-8')

    def content(self):
        if not self.memory:
            self.retrieve()
        return print(self.contenido)

    def show(self):
        self.content()


if __name__ == '__main__':
    r = Robot('https://labs.eif.urjc.es/')
    print('-----Comprobamos el uso de las funciones de esta clase-------')
    print('Retrieve')
    r.retrieve()
    print('Show')
    r.show()
    print('Content')
    r.content()
